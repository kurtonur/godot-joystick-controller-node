extends KinematicBody2D


export(float) var speed = 300

export(NodePath) var joystickLeftPath
onready var joystickLeft : Joystick = get_node(joystickLeftPath)

export (NodePath) var joystickRightPath
onready var joystickRight : Joystick = get_node(joystickRightPath)

func _physics_process(delta: float) -> void:
	move(delta)

func move(delta: float) -> void:
	
	if joystickLeft and joystickLeft.getIsReady() and joystickLeft.IsUsing():
		#rotation = joystickLeft.getValue().angle()
		move_and_slide(joystickLeft.getValue() * speed)
		
	if joystickRight and joystickRight.getIsReady() and joystickRight.IsUsing():
		if joystickRight.getValue().length() != 0:
			rotation = joystickRight.getValue().angle()

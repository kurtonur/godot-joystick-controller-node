# README #

  

This README would normally document whatever steps are necessary to get your application up and running.

  

### What is this repository for? ###

  

* You are able to quickly set a joystick to your godot project.

* Godot 3.2

  

### How do I get set up? ###

  

* Copy joystick folder to your project folder.

* Add  a new node of joystick

	![https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/464fa1882a64616282e2b6155a0e640f852d45e2/images/img1.png](https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/464fa1882a64616282e2b6155a0e640f852d45e2/images/img1.png)

* you will see in scene menu

	![https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/464fa1882a64616282e2b6155a0e640f852d45e2/images/img2.png](https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/464fa1882a64616282e2b6155a0e640f852d45e2/images/img2.png)
 
 * Then Edit some attributes
 
	![https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/464fa1882a64616282e2b6155a0e640f852d45e2/images/img3.png](https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/464fa1882a64616282e2b6155a0e640f852d45e2/images/img3.png)
  
* you have to set joystick Back, joystick Button, and joystick Click.

* Moreover, you see on godot editor if you set Texture of joystick (Optional)

* Result ->

	![https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/464fa1882a64616282e2b6155a0e640f852d45e2/images/img4.png](https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/464fa1882a64616282e2b6155a0e640f852d45e2/images/img4.png)

	![https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/4015a292393e49e86d687dbafd19873552dd714c/images/img5.png](https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/4015a292393e49e86d687dbafd19873552dd714c/images/img5.png)

	![https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/4015a292393e49e86d687dbafd19873552dd714c/images/img6.png](https://bitbucket.org/kurtonur/godot-joystick-controller-node/raw/4015a292393e49e86d687dbafd19873552dd714c/images/img6.png)

*		get_value() function return a Vector2D to show direction of joystick

* Demo folder is already ready to test joystick

#### Extra ####
Don`t forget to check Emulate Touch From Mouse in Project\General\Input Devices\Pointing if you want to test on editor or Windows
 
### References ###

*  [Gonkee](https://github.com/Gonkee/Gonkees-Shaders)
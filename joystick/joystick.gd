tool
extends Sprite

class_name Joystick, "res://joystick/joystick.png"

export(Texture) var joystick_back
export(Texture) var joystick_button
export(Texture) var joystick_click 

export(Vector2) var radius = Vector2(40, 40)
export(int,1,100,1) var boundary = 64
export(int,1,100,1) var return_accel = 20
export(int,1,50,1) var threshold = 10
var index = -1

onready var button = TouchScreenButton.new()

var isready = true

func _ready():
	if(!joystick_back):
		isready = false
		return
	self.texture = joystick_back
	self.setVariables()
	button.position = button.position - radius
	
	self.add_child(button)
	pass

func _process(delta):
	if index == -1:
		var pos_difference = (Vector2.ZERO - radius) - button.position
		button.position += pos_difference * return_accel * delta
	pass

func getButtonPos():
	return button.position + radius
	pass


func _input(event):
	if(!joystick_button or !joystick_click or !joystick_back):
		isready = false
		return
	buttonMove(event)
	pass

func buttonMove(event):
	if event is InputEventScreenDrag or (event is InputEventScreenTouch and event.is_pressed()):
		var event_dist_from_centre = (event.position - button.get_parent().global_position).length()
		if event_dist_from_centre <= boundary * button.global_scale.x or event.get_index() == index:
			button.set_global_position(event.position - radius * button.global_scale)
			if getButtonPos().length() > boundary:
				button.set_position( getButtonPos().normalized() * boundary - radius)
			index = event.get_index()
	if event is InputEventScreenTouch and !event.is_pressed() and event.get_index() == index:
		index = -1
	pass

func getValue():
	if(!joystick_button or !joystick_click or !joystick_back):
		isready = false
		return Vector2.ZERO
	if getButtonPos().length() > threshold:
		return getButtonPos().normalized()
	return Vector2.ZERO
	pass
	
func setVariables():
	if(!joystick_button or !joystick_click):
		isready = false
		return
	button.normal = joystick_button
	button.pressed = joystick_click
	pass


func IsUsing():
	if index != -1:
		return true
	else:
		return false
	pass

func getIsReady():
	return isready
	pass
